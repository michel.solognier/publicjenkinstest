def buildApp() {
    echo 'building the application'
    curl --location --request POST 'http://localhost:9999/query' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data-urlencode 'q=CREATE DATABASE postmanJENK'
}

def testApp() {
    echo 'testing the application'
}

def deployApp() {
    echo 'deploying the application'
}


return this